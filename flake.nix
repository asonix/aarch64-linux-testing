{
  description = "Aarch64 image with latest kernel";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/master";
  outputs = { self, nixpkgs }: rec {
    nixosConfigurations.installer = ftd: nixpkgs.lib.nixosSystem {
      system = "aarch64-linux";
      modules = [
        "${nixpkgs}/nixos/modules/installer/sd-card/sd-image-aarch64.nix"
        (
          let
            pkgs = import nixpkgs
              {
                system = "aarch64-linux";
              };
            linux-testing = pkgs.linuxKernel.kernels.linux_testing;
            linux-testing-rockchip = pkgs.linuxKernel.packagesFor (linux-testing.override {
              structuredExtraConfig = with nixpkgs.lib.kernel; {
                SND_SOC_RK817 = yes;
                STMMAC_ETH = yes;
                MOTORCOMM_PHY = yes;
                MMC_DW = yes;
                MMC_DW_ROCKCHIP = yes;
                MMC_SDHCI_OF_DWCMSHC = yes;
                PCIE_ROCKCHIP_DW_HOST = yes;
                PHY_ROCKCHIP_NANENG_COMBO_PHY = yes;
                ROCKCHIP_DW_HDMI = yes;
                PHY_ROCKCHIP_INNO_DSIDPHY = yes;
                ROCKCHIP_VOP2 = yes;
                ARCH_ROCKCHIP = yes;
                ROCKCHIP_PHY = yes;
                PHY_ROCKCHIP_INNO_USB2 = yes;
                RTC_DRV_RK808 = yes;
                COMMON_CLK_RK808 = yes;
                MFD_RK808 = yes;
                CHARGER_RK817 = yes;
                REGULATOR_RK808 = yes;
                ROCKCHIP_PM_DOMAINS = yes;
                GPIO_ROCKCHIP = yes;
                PINCTRL_ROCKCHIP = yes;
                PWM_ROCKCHIP = yes;
                ROCKCHIP_IOMMU = yes;
                ROCKCHIP_MBOX = yes;
                ROCKCHIP_SARADC = yes;
                ROCKCHIP_THERMAL = yes;
                SPI_ROCKCHIP = yes;
                VIDEO_HANTRO_ROCKCHIP = yes;
                ROCKCHIP_IODOMAIN = yes;
                COMMON_CLK_ROCKCHIP = yes;
                PHY_ROCKCHIP_INNO_CSIDPHY = yes;
              };
            });
          in
          {
            nixpkgs.overlays = [
              (final: super: {
                zfs = super.zfs.overrideAttrs (_: {
                  meta.platforms = [ ];
                });
              })
            ];

            users.users.root.initialHashedPassword = "";
            users.users.root.openssh.authorizedKeys.keys = [
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICTz/raHvMluy7wPhO1P64ZgyVu7ZQrRgKL0CT8UcOdQ asonix@firestar"
              "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBlkVa6HyTPIoJoieFc9seM5Y31iaRsgHH3xH2tlxCSV asonix@graystripe"
            ];

            services.openssh.enable = true;

            networking.hostName = "nixos-aarch64";
            boot.kernelPackages = linux-testing-rockchip;
            boot.kernelParams = [ "console=ttyS2,1500000n8" ];
            hardware.deviceTree.name = ftd; # "rockchip/rk3566-soquartz-blade.dtb";
            system.stateVersion = "23.05";
          }
        )
      ];
    };
    images.installer = ftd: (nixosConfigurations.installer ftd).config.system.build.sdImage;
    images.soquartz-installer = images.installer "rockchip/rk3566-soquartz-blade.dtb";
  };
}
